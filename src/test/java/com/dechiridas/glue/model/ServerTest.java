package com.dechiridas.glue.model;

import org.junit.Before;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.*;

public class ServerTest {

    public InetAddress address;
    public Server server;

    @Before
    public void before() throws UnknownHostException {
        address = InetAddress.getByName("191.168.1.20");
        server = new Server(address);
    }

    @Test
    public void getAddress() {
        InetAddress serverAddress = server.getAddress();
        assertEquals("Expected value should be: " + address.toString(), address, serverAddress);
    }

    @Test
    public void equals() throws UnknownHostException {
        Server server2 = new Server(address);
        assertEquals("These objects should equal each other", server, server2);
        Server server3 = new Server(InetAddress.getByName("192.168.1.21"));
        assertNotEquals("These objects should not equal each other", server, server3);
    }
}