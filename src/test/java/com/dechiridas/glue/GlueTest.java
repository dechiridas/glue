package com.dechiridas.glue;

import com.dechiridas.glue.model.Server;
import org.junit.Before;
import org.junit.Test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.*;

public class GlueTest {

    private Glue glue = new Glue();
    private Server server1, server2, server3;

    @Before
    public void before() throws UnknownHostException {
        glue.start(true);
        server1 = new Server(InetAddress.getByName("192.168.1.20"));
        server2 = new Server(InetAddress.getByName("192.168.1.21"));
        server3 = new Server(InetAddress.getByName("192.168.1.22"));
        glue.addServer(server1);
    }

    @Test
    public void getServers() {
        Server[] servers = new Server[] { server1 };
        assertArrayEquals("Servers array should only contain 1 entry.", glue.getServers(), servers);
    }

    @Test
    public void addServer() {
        Server[] servers = new Server[] { server1, server2 };
        glue.addServer(server2);
        assertArrayEquals("Servers array should now have 2 entries.", glue.getServers(), servers);
    }

    @Test
    public void removeServer() {
        Server[] servers = new Server[] { server1 };
        glue.removeServer(server2);
        assertArrayEquals("Servers array should only contain 1 entry again.", glue.getServers(), servers);
    }
}