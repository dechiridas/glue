package com.dechiridas.glue.model;

import java.net.InetAddress;

public class Server {

    private InetAddress address;

    public Server(InetAddress address) {
        this.address = address;
    }

    public InetAddress getAddress() {
        return address;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Server)) {
            return false;
        }

        Server srv = (Server)obj;

        if (!srv.getAddress().toString().equals(this.getAddress().toString())) {
            return false;
        }

        return true;
    }
}
