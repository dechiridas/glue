package com.dechiridas.glue.ref;

public class Strings {
    public static final String initMessage =
            "Project Glue is an attempt at creating a remote multinode, clustered Minecraft server experience. The " +
            "servers involved are linked and must maintain consistence to work with Glue. Servers will need to " +
            "install a separate Glue plugin to make use of this servers services.";
}
