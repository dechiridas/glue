package com.dechiridas.glue;

import com.dechiridas.glue.model.Server;
import com.dechiridas.glue.net.DiscardServerHandler;
import com.dechiridas.glue.ref.Strings;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Glue {

    public static Glue instance;
    private Server[] servers = {};
    private Logger logger = Logger.getLogger(Glue.class.getName());
    private int port;

    public static void main(String[] args) {
        System.out.println(Strings.initMessage);

        instance = new Glue();
        instance.start();
    }

    public void start(boolean noStart) {
        port = 9001;
        logger.log(Level.INFO, "Starting Glue server on port: " + port + " ...");
    }

    public void start() {
        port = 9001;
        logger.log(Level.INFO, "Starting Glue server on port: " + port + " ...");

        try {
            run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) {
                            ch.pipeline().addLast(new DiscardServerHandler());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            ChannelFuture future = bootstrap.bind(port).sync();

            future.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }

    }

    public Logger getLogger() {
        return logger;
    }

    public Server[] getServers() {
        return this.servers;
    }

    public boolean addServer(Server server) {
        if (hasServer(server)) {
            return false;
        }

        Server[] newServersArray = new Server[servers.length + 1];

        for (int i = 0; i < servers.length; i++) {
            newServersArray[i] = servers[i];
        }

        newServersArray[newServersArray.length - 1] = server;
        this.servers = newServersArray;

        return true;
    }

    public boolean removeServer(Server server) {
        if (!hasServer(server)) {
            return false;
        }

        Server[] newServersArray = new Server[servers.length - 1];

        int i = 0;
        for (Server srv : servers) {
            if (!srv.equals(server)) {
                newServersArray[i] = servers[i];
                i++;
            }
        }

        return true;
    }

    private boolean hasServer(Server server) {
        for (Server srv : servers) {
            if (srv.equals(server)) {
                return true;
            }
        }

        return false;
    }

}
